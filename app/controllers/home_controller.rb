class HomeController < ApplicationController
  before_action :require_login

  def index
  end
  
  def club_my
    
    @c = UsersClub.where(user_id: current_user.id)
  end
  
  def club_detail
    @detail = Club.find(params[:id])
  end
  
  
  def club_make_view
  end
  
  def club_make
    c = Club.new(name:params[:club_name], about:params[:club_about], profile_pic:params[:club_profile], cover_pic:params[:club_cover])
    c.save
    
    unless UsersClub.where(user_id:current_user.id).where(club_id: c.id).first
      k = UsersClub.new
      k.user_id = current_user.id
      k.club_id = c.id
      k.save
    end
    
    redirect_to "/home/club_show"
  end
  
  def club_join
    
    unless UsersClub.where(user_id:current_user.id).where(club_id: params[:id]).first
      k = UsersClub.new
      k.user_id = current_user.id
      k.club_id = params[:id]
      k.save
    end
    
    
    redirect_to "/home/club_detail/#{params[:id]}"    
  end
  
  def club_destory
    d = Club.find(params[:id])
    d.destroy
    
    redirect_to :back
  end
  
  def club_show
    @club = Club.all
  end
  
  def festival_all
    @k = params[:id]
    @f = Club.find(params[:id])
    
  end
  
  def festival_make
    f = Festival.new(club_id:params[:club_id], name:params[:festival_name], about:params[:festival_about], date:params[:festival_date], festival_pic:params[:festival_pic])
    f.save
    
    redirect_to :back
  end
  
  def recruit_all
    @k = params[:id]
    @f = Club.find(params[:id])
  end   
  
  def recruit_make
    f = Recruit.new(club_id:params[:club_id], name:params[:recruit_name], about:params[:recruit_about], date:params[:recruit_date], recruit_pic:params[:recruit_pic])
    f.save
    
    redirect_to :back
  end
  
end
