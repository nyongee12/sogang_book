class Club < ActiveRecord::Base
    belongs_to :c_category
    has_many :festivals
    has_many :recruits
    
    has_many :users_clubs
    has_many :users, through: :users_clubs
end
