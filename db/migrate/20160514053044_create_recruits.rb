class CreateRecruits < ActiveRecord::Migration
  def change
    create_table :recruits do |t|
      t.string :club_id
      t.string :name
      t.string :about
      t.datetime :date
      t.string :recruit_pic

      t.timestamps null: false
    end
  end
end
