class CreateCCategories < ActiveRecord::Migration
  def change
    create_table :c_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
