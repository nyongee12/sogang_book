class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name
      t.string :about
      t.string :profile_pic
      t.string :cover_pic
      t.integer :c_category_id

      t.timestamps null: false
    end
  end
end
