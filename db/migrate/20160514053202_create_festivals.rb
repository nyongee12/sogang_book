class CreateFestivals < ActiveRecord::Migration
  def change
    create_table :festivals do |t|
      t.string :club_id
      t.string :name
      t.string :about
      t.datetime :date
      t.string :festival_pic

      t.timestamps null: false
    end
  end
end
